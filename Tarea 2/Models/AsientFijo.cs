﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tarea_2.Models
{
    public enum Movimiento
    {
        CR, DB
    }

    public class AsientFijo
    {
        //Detalle
        [Key]
        public int Numero { get; set; }
        [Required]
        public string Descripcion { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime FechaDetalle { get; set; }
        [Required]
        public int CuentaContable { get; set; }
        [Required]
        public Movimiento TipoMovimiento { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal Monto { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal Descuento { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal SubTotal { get; set; }

        public virtual int? Referencia { get; set; }

        public virtual string ReferenciaDescripcion { get; set; }
    }
}