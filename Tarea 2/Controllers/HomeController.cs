﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tarea_2.Models;

namespace Tarea_2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ImportJson()
        {
            string path = @"D:\AsientoFijo.json";

            if (System.IO.File.Exists(path))
            {
                using (StreamReader r = new StreamReader(path))
                {
                    string json = r.ReadToEnd();

                    var array = JsonConvert.DeserializeObject<IEnumerable<AsientFijo>>(json);

                    return View("Index", array);
                }
            }
            else 
            {
                return RedirectToAction(@"Shared/Error");
            }
        }
    }
}