﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Creador_Json.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Creador_Json.Controllers
{
    public class HomeController : Controller
    {
        private AientoFijoDbContext db = new AientoFijoDbContext();

        public ActionResult Index()
        {
            return View(db.AsientosFijos.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "Numero,Descripcion,FechaDetalle,CuentaContable,TipoMovimiento,Monto,Descuento,SubTotal,Referencia,ReferenciaDescripcion")] AsientFijo asientoFijo)
        {
            if (ModelState.IsValid)
            {
                db.AsientosFijos.Add(asientoFijo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(asientoFijo);
        }

        public ActionResult ExportJson()
        {
            //db.AsientosFijos.Add();
            //db.SaveChanges();

            string objjsonData = JsonConvert.SerializeObject(db.AsientosFijos);

            string path = @"D:\AsientoFijo.json";

            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);

                using (var tw = new StreamWriter(path, true))
                {
                    tw.WriteLine(objjsonData.ToString());
                    tw.Close();
                }
                return RedirectToAction("Index");
            }
            else if (!System.IO.File.Exists(path))
            {
                using (var tw = new StreamWriter(path, true))
                {
                    tw.WriteLine(objjsonData.ToString());
                    tw.Close();
                }
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}