﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Creador_Json.Models
{
    public class AientoFijoDbContext : DbContext
    {
        public AientoFijoDbContext()
        {
        }

        public AientoFijoDbContext(string connectionString) : base(connectionString)
        {
            Database.SetInitializer<AientoFijoDbContext>(null);
        }

        public DbSet<AsientFijo> AsientosFijos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}